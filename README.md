# Hash Code 2020

Our solutions for the 2020 HashCode problems. 
[extended_round.ipynb](extended_round.ipynb) contains the algorithms we tried so far.
For reference, [in](in) contains the problem instances.

## Best solutions found so far

| Input                      | Score      |
| -----------                | ----------:|
| A – example                |         21 |
| B – read on                |  5,822,900 | 
| C – incunabula             |  5,689,822 |
| D – tough choices          |  5,039,320 |
| E – so many books          |  5,095,171 |
| F – libraries of the world |  5,343,950 |
| **Total score**            | **26,991,184** |
